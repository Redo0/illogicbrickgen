--love 1

local lg = love.graphics

--local imageFixIcon = dofile "../blBrickIconGenerator/imagefixicon.lua"
local imageFixIcon = require("imagefixicon")

local function unquote(s)
	return s:match("^\"(.*)\"$")
end

local function brickDataToIcon(data, identifier)
	local w = data.logicBrickSize[1]
	local h = data.logicBrickSize[2]
	local d = data.logicBrickSize[3]*2/5
	
	local isvertical = w==1 and data.logicBrickSize[3]>1
	
	local canvas
	if data.isLogicWire then
		if isvertical then
			canvas = lg.newCanvas((h+2)*16, (d+2)*16)
		else
			canvas = lg.newCanvas((h+2)*16, (w+2)*16)
		end
	elseif data.isLogicGate then
		canvas = lg.newCanvas((w+2)*16, (h+2)*16)
	else
		error("data "..data.uiName.." is not a gate or wire")
	end
	
	lg.setCanvas(canvas) lg.push()
		lg.clear()
		
		lg.setColor(1,0,1)
		lg.rectangle("fill", 0, 0, canvas:getWidth(), canvas:getHeight())
		
		lg.translate(canvas:getWidth()/2, canvas:getHeight()/2)
		lg.scale(16, 16)
		
		if data.isLogicWire then
			
			if isvertical then
				lg.setColor(0.75,0.75,0.75,1)
				lg.rectangle("fill", -h/2, -d/2, h, d)
			else
				for xi = 1, h do
					for yi = 1, w do
						local x = xi - h/2 - 0.5
						local y = yi - w/2 - 0.5
						lg.translate(x, y)
							lg.setColor(0.6,0.6,0.6,1)
							lg.rectangle("fill", -0.25, -0.25, 0.5, 0.5) --center
							lg.setColor(0.55,0.55,0.55,1)
							lg.polygon("fill", -0.25, -0.25, -0.25, 0.25, -0.5, 0.5, -0.5, -0.5) --left
							lg.setColor(0.5,0.5,0.5,1)
							lg.polygon("fill", -0.25, -0.25, 0.25, -0.25, 0.5, -0.5, -0.5, -0.5) --top
							lg.setColor(0.65,0.65,0.65,1)
							lg.polygon("fill", 0.25, 0.25, 0.25, -0.25, 0.5, -0.5, 0.5, 0.5) --right
							lg.setColor(0.65,0.65,0.65,1)
							lg.polygon("fill", 0.25, 0.25, -0.25, 0.25, -0.5, 0.5, 0.5, 0.5) --bottom
						lg.translate(-x, -y)
					end
				end
			end
			
		elseif data.isLogicGate then
			
			lg.rotate(data.orientationFix*math.pi/2 + math.pi/2)
			
			lg.setColor(0.75,0.75,0.75,1)
			lg.rectangle("fill", -w/2, -h/2, w, h)
			
			lg.setColor(0.9,0.9,0.9,1)
			for portidx, port in ipairs(data.ports) do
				local tx = port.pos[1]/2
				local ty = -port.pos[2]/2
				local tr
				if port.dir==0 then
					tx = tx-1
					tr = 0
				elseif port.dir==2 then
					tx = tx+1
					tr = math.pi
				elseif port.dir==1 then
					ty = ty-1
					tr = math.pi/2
				elseif port.dir==3 then
					ty = ty+1
					tr = math.pi/2*3
				end
				if tr then
					tr = tr-math.pi/2
					
					lg.translate(tx, ty)
						lg.rotate(tr)
							if port.type==1 then
								lg.rectangle("fill", -0.4, 0.3, 0.8, 0.2)
							elseif port.type==0 then
								lg.polygon("fill", -0.4, 0.5, 0.4, 0.5, 0.3, 0.2, -0.3, 0.2)
							end
						lg.rotate(-tr)
					lg.translate(-tx, -ty)
				end
			end
		end
		
		lg.scale(1/16, 1/16)
		lg.translate(-256, -256)
		
	lg.pop() lg.setCanvas()
	
	local idlayout = canvas:newImageData()
	
	local idicon = imageFixIcon(idlayout, identifier, false)
	
	local dicon = lg.newImage(idicon)
	
	local cfinal = lg.newCanvas(96, 96)
	
	lg.setCanvas(cfinal) lg.push()
		lg.clear()
		lg.setColor(1, 1, 1, 1)
		lg.draw(dicon)
		lg.setFont(lg.newFont(5))
		lg.print(unquote(data.uiName), 2, 2)
	lg.pop() lg.setCanvas()
	
	local idfinal = cfinal:newImageData()
	
	--if YieldImage then YieldImage(idlayout) end
	if YieldImage then YieldImage(idfinal) end
	
	return idfinal:encode("png"):getString()
end

return brickDataToIcon
