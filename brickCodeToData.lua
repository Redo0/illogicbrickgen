
local ibd = {}

local function unquote(s)
	return s:match("^\"(.*)\"$")
end

local function parammatchstr(param)
	return "logicPort"..param.."%[[^%]]+%] = ([^;]+);"
end

local function vecfromstr(str)
	local vec = {}
	for val in str:gmatch("[0-9%.%-]+") do
		table.insert(vec, tonumber(val))
	end
	return vec
end

local function brickCodeToData(intext)
	intext = intext:gsub("\r", "")
	
	local strpre = intext:match("^.+numLogicPorts[^\n]+")
	if not strpre then
		strpre = intext:match("^[^%}]+")
	end
	strpre = strpre.."\n"
	strpre = strpre:gsub("\t", "")
	
	local data = {}
	for propname, propval in strpre:gmatch("\n([a-zA-Z]+) = ([^;]+);") do
		local val
		
		if propname=="logicBrickSize" then
			val = vecfromstr(propval)
		elseif propval:find("^\".*\"$") then
			--val = unquote(propval)
			val = propval
		elseif propval=="true" then
			val = true
		elseif propval=="false" then
			val = false
		elseif tonumber(propval) then
			val = tonumber(propval)
		else
			val = propval
			--error("invalid data type for "..propname.." = \""..propval.."\"")
		end
		
		data[propname] = val
	end
	
	data.ports = {}
	
	for portstr in intext:gmatch("logicPortType.-logicPortUIName[^\n]+") do
		portstr = portstr:gsub("\t", "")
		
		local port = {
			idx  = tonumber  (portstr:match("%[([^%]]+)%]"         )),
			type = tonumber  (portstr:match(parammatchstr("Type")  )),
			pos  = vecfromstr(portstr:match(parammatchstr("Pos")   )),
			dir  = tonumber  (portstr:match(parammatchstr("Dir")   )),
			name = unquote   (portstr:match(parammatchstr("UIName"))),
		}
		
		table.insert(data.ports, port)
	end
	
	return data
end

return brickCodeToData
