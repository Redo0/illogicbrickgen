--love 1

local le = love.event
local lg = love.graphics
local li = love.image
local lf = love.filesystem

--------------------------------------------------------------

local brickCodeToData = require "brickCodeToData"
local brickDataToIcon = require "brickDataToIcon"

--------------------------------------------------------------

local icons_done = false

local drawable
function YieldImage(imagedata)
	drawable = lg.newImage(imagedata)
	coroutine.yield()
end

local function unquote(s)
	return s:match("^\"(.*)\"$")
end

local function textLoad(filename)
	local infile = io.open(filename, "rb")
	local intext = infile:read("*a")
	infile:close()
	return intext
end

local function writeToFile(filename, text)
	local outfile = io.open(filename, "wb")
	outfile:write(text)
	outfile:close()
end

--------------------------------------------------------------

local function genAllIcons()
	local files = lf.getDirectoryItems("output/newcode")
	for fileidx, filename in ipairs(files) do
		local fileext = filename:match("%.[a-zA-Z0-9]+$")
		if fileext==".cs" then
			local intext = textLoad("output/newcode/"..filename)
			local data = brickCodeToData(intext)
			
			local png = brickDataToIcon(data, fileidx)
			
			writeToFile("output/newicons/"..unquote(data.uiName)..".png", png)
			
			print(unquote(data.uiName))
		end
	end
	
	print("Done")
	
	icons_done = true
end

--------------------------------------------------------------

local cor
function love.load()
	cor = coroutine.create(genAllIcons)
	--genAllIcons()
end

function love.draw()
	if drawable then
		lg.draw(drawable)
		lg.rectangle("line", 0, 0, drawable:getWidth(), drawable:getHeight())
	end
end

function love.update(dt)
	if cor then coroutine.resume(cor) end
	
	if icons_done then le.quit() end
end

function love.keypressed(k)
	if k=="escape" then le.quit()
	elseif k=="r" then if cor then coroutine.resume(cor) end
	end
end
