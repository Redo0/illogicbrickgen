
local function vectostr(vec)
	return table.concat(vec, " ")
end

local dbPropIgnore = {}

local dbPropsGate = {
	{"brickFile"       , nil},
	{"iconName"        , nil},
	{},
	{"category"        , nil},
	{"subCategory"     , nil},
	{"uiName"          , nil},
	{"logicUIName"     , nil},
	{"logicUIDesc"     , nil},
	{},
	{"hasPrint"        , nil},
	{"printAspectRatio", nil},
	{"logicForceColor" , dbPropIgnore},
	{"logicForcePrint" , dbPropIgnore},
	{},
	{"logicBrickSize"  , nil},
	{"orientationFix"  , 0},
	{},
	{"isLogic"         , nil},
	{"isLogicGate"     , nil},
	{"isLogicInput"    , false},
	{},
	{"logicInit"       , "\"\""},
	{"logicInput"      , "\"\""},
	{"logicUpdate"     , "\"\""},
	{"logicGlobal"     , "\"\""},
	{},
	{"numLogicPorts"   , nil},
}

local dbPropsWire = {
	{"brickFile"       , nil},
	{"iconName"        , nil},
	{},
	{"category"        , nil},
	{"subCategory"     , nil},
	{"uiName"          , nil},
	{},
	{"logicBrickSize"  , nil},
	{"orientationFix"  , 0},
	{},
	{"isLogic"         , nil},
	{"isLogicWire"     , nil},
}

local function convertValue(val)
	if type(val)=="boolean" then
		val = val and "true" or "false"
	elseif type(val)=="string" then
		--propval = "\""..propval.."\""
	elseif type(val)=="table" then
		val = "\""..vectostr(val).."\""
	elseif type(val)=="number" then
		
	else
		error("invalid type "..type(val))
	end
	return val
end

local function brickDataToCode(data)
	local tdatatext = {}
	
	local proplist
	if data.isLogicWire then
		proplist = dbPropsWire
	elseif data.isLogicGate then
		proplist = dbPropsGate
	else
		error("data "..data.uiName.." is not a gate or a wire")
	end
	
	for propdefidx, propdef in ipairs(proplist) do
		local propname = propdef[1]
		if propname then
			local propval = data[propname]
			if not propval then
				propval = propdef[2]
			end
			if propval==nil then error("no propval for "..propname.." in brick "..data.uiName) end
			if propval~=dbPropIgnore then
				table.insert(tdatatext, propname.." = "..convertValue(propval)..";")
			end
		else
			table.insert(tdatatext, "")
		end
	end
	
	local tporttext = {}
	if data.isLogicGate then
		if not data.ports then error("no ports in brick "..data.uiName) end
		for portidx, port in ipairs(data.ports) do
			local str = string.format(
				"logicPortType[%i] = %i;"..
				"logicPortPos[%i] = \"%s\";"..
				"logicPortDir[%i] = %i;"..
				"logicPortUIName[%i] = \"%s\";",
				portidx-1, port.type,
				portidx-1, vectostr(port.pos),
				portidx-1, port.dir,
				portidx-1, port.name
			)
			if port.upd then str = str..string.format(
				"logicPortCauseUpdate[%i] = true;",
				portidx-1
			) end
			str = str:gsub(";", ";\n\t")
			table.insert(tporttext, str)
		end
	end
	
	local textradatatext = {}
	if data.extraData then
		for name, val in pairs(data.extraData) do
			table.insert(textradatatext, name.." = "..convertValue(val)..";");
		end
	end
	
	local outtext = {
		"",
		"datablock fxDtsBrickData("..data.dbname.."){",
		"\t"..table.concat(tdatatext, "\n\t"),
		"\t",
		"\t"..table.concat(textradatatext, "\n\t"),
		"\t",
		"\t"..table.concat(tporttext, "\n\t"),
		"};",
		(data.extraCode or "")
	}
	
	return table.concat(outtext, "\n")
end

return brickDataToCode
