
return function(gate, args)
	local data = args[1]
	for i = 1, #data do
		local c = data:sub(i, i)
		local v = (c=="1") and 1 or 0
		gate.romdata[i-1] = v
		Gate.setcdata(gate, i-1, v)
	end
	Gate.queue(gate, 0)
end
