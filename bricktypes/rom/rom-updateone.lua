
return function(gate)
	if Gate.getportstate(gate, $enableport)~=0 then
		Gate.setportstate(gate, $dataport, gate.romdata[$getaddr])
	else
		Gate.setportstate(gate, $dataport, 0)
	end
end
