
return function(gate)
	if Gate.getportstate(gate, $enableport)~=0 then
		local addr = $getaddr
		for i = 0, $databitsm1 do
			Gate.setportstate(gate, $dataport+i, gate.romdata[addr+$layerbits*i])
		end
	else
		for i = $dataport, $dataportend do
			Gate.setportstate(gate, i, 0)
		end
	end
end
