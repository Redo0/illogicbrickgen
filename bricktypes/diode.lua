
local function logicUpdateGenDiode(inv)
	local tfunc = {
		"return function(gate)",
		"	Gate.setportstate(gate, 1, "..(inv and "1 - " or "").."Gate.getportstate(gate, 2))",
		"end",
	}
	return LogicStrFromTable(tfunc)
end

local function genDiode(dirname, inv)
	local data = {}
	
	local dirui = dirname:sub(1,1):upper()..dirname:sub(2, #dirname)
	local kindname = inv and "Inverter" or "Diode"
	
	local dbname = "LogicGate_"..kindname..dirui.."_Data"
	local uiname = kindname.." "..dirui
	local subcat = "Diode"
	
	GenDataStandard(data, dbname, uiname, subcat)
	GenDataGate(data)
	
	local arrowdirs = {
		["flat"] = "ARROW",
		["up"  ] = "UPARROW",
		["down"] = "DOWNARROW",
	}
	data.logicForceColor = "\""..(inv and "RED" or "GREEN").."\""
	data.logicForcePrint = "\""..arrowdirs[dirname].."\""
	
	data.logicBrickSize = {1, 1, 1}
	data.orientationFix = 3
	
	data.logicUpdate = logicUpdateGenDiode(inv)
	
	local portdirs = {
		["flat"] = {3, 1},
		["up"  ] = {5, 4},
		["down"] = {4, 5},
	}
	
	data.numLogicPorts = 2
	
	data.ports = {}
	data.ports[1] = {
		type = 1,
		pos = {0, 0, 0},
		dir = portdirs[dirname][1],
		name = "In",
		upd = true,
	}
	data.ports[2] = {
		type = 0,
		pos = {0, 0, 0},
		dir = portdirs[dirname][2],
		name = "Out",
	}
	
	return data
end


local diodedirs = {"flat", "up", "down"}
for diridx, dir in ipairs(diodedirs) do
	OutputBrickData(genDiode(dir, false))
	OutputBrickData(genDiode(dir, true ))
end
