
local function logicUpdateGenRom(addrbits, databits)
	local getaddrt = {"("}
	table.insert(getaddrt, "			(Gate.getportstate(gate, 1))")
	for i = 1, addrbits-1 do
		table.insert(getaddrt, "			+ (Gate.getportstate(gate, "..(i+1)..") * "..math.pow(2, i)..")")
	end
	table.insert(getaddrt, "		)")
	return LogicStrFromFile("bricktypes/rom/"..(databits==1 and "rom-updateone.lua" or "rom-updatemany.lua"), {
		getaddr = table.concat(getaddrt, "\n"),
		dataport = addrbits+1,
		dataportend = addrbits+databits,
		enableport = addrbits+databits+1,
		databitsm1 = databits-1,
		layerbits = math.pow(2, addrbits),
	})
end

local function logicInitGenRom(addrbits, databits)
	return LogicStrFromFile("bricktypes/rom/rom-init.lua", {
		totalbitsm1 = (math.pow(2, addrbits) * databits) - 1,
	})
end

local function logicInputGenRom(addrbits, databits)
	return LogicStrFromFile("bricktypes/rom/rom-input.lua", {})
end

local function genRom(size)
	size.x, size.y, size.z = size[1], size[2], (size[3] or 1)
	assert(IsPower2(size.x) and IsPower2(size.y))
	assert(size.x >= size.z)
	
	local data = {}
	
	local dbname = "LogicGate_Rom"..(size.x).."x"..(size.y).."x"..(size.z).."_Data"
	local uiname = "ROM "..(size.x).."x"..(size.y)..((size.z~=1) and ("x"..(size.z)) or "")
	local subcat = "ROM"
	
	GenDataStandard(data, dbname, uiname, subcat)
	GenDataGate(data)
	
	data.logicBrickSize = {size.x, size.y, 1}
	data.orientationFix = 3
	
	local addrbits = math.log(size.x*size.y)/math.log(2)
	local databits = size.z
	
	data.logicUpdate = logicUpdateGenRom(addrbits, databits)
	data.logicInit   = logicInitGenRom  (addrbits, databits)
	data.logicInput  = logicInputGenRom (addrbits, databits)
	
	data.numLogicPorts = addrbits + databits + 1
	
	data.ports = {}
	-- addr inputs
	for i = 1, addrbits do
		data.ports[i] = {
			type = Input,
			pos = {(size.x-1) - (i-1)*2, -(size.y-1), 0},
			dir = Toward,
			name = "A"..(i-1),
			upd = false,
		}
	end
	-- data outputs
	for i = 1, databits do
		data.ports[addrbits+i] = {
			type = Output,
			pos = {(size.x-1) - (i-1)*2, (size.y-1), 0},
			dir = Away,
			name = "O"..(i-1),
		}
	end
	-- enable input
	data.ports[addrbits+databits+1] = {
		type = Input,
		pos = {size.x-1, -(size.y-1), 0},
		dir = Right,
		name = "Clock",
		upd = true,
	}
	
	data.extraData = {}
	data.extraData.isLogicRom = true
	data.extraData.logicRomX = size.x
	data.extraData.logicRomY = size.y
	data.extraData.logicRomZ = size.z
	
	--data.extraCode = CodeStrFromFile("bricktypes/rom/rom.cs", {
	--	dbname = dbname,
	--})
	
	return data
end


local romsizes = {
	
	-- useful:
	-- 8d x 4a: 7 seg rom
	-- 1d x 8a: old computer
	-- 64d x 10a: new computer
	-- 32d x 8a: 4x8 font
	-- 64d x 8a: 8x8 font
	-- 4d x 8+3=11a: 4x8 font
	-- 8d x 8+3=11a: 8x8 font
	
	--  1 bit data    4 bit data    8 bit data   16 bit data   32 bit data   48 bit data   64 bit data
	  { 4,  4    }, { 4,  4,  4}, { 8,  2,  8},                                                         --  4 bit addr
	  { 8,  8    }, { 8,  8,  4}, { 8,  8,  8}, {16,  4, 16}, {32,  2, 32}, {64,  1, 48}, {64,  1, 64}, --  6 bit addr
	  {16, 16    }, {16, 16,  4}, {16, 16,  8}, {16, 16, 16}, {32,  8, 32}, {64,  4, 48}, {64,  4, 64}, --  8 bit addr
	  {32, 16    }, {32, 16,  4}, {32, 16,  8}, {32, 16, 16}, {32, 16, 32}, {64,  8, 48}, {64,  8, 64}, --  9 bit addr
	                              {32, 32,  8}, {32, 32, 16}, {32, 32, 32}, {64, 16, 48}, {64, 16, 64}, -- 10 bit addr
	                              {64, 32,  8}, {64, 32, 16}, {64, 32, 32}, {64, 32, 48}, {64, 32, 64}, -- 11 bit addr
	                              {64, 64,  8}, {64, 64, 16}, {64, 64, 32}, {64, 64, 48}, {64, 64, 64}, -- 12 bit addr
}
for szidx, size in ipairs(romsizes) do
	OutputBrickData(genRom(size))
end
