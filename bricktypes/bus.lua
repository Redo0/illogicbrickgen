
local function logicUpdateGenBus(bits, kind, activelow)
	local clockport = bits*2 + 1
	
	local tfunc = {
		"return function(gate)",
		"	if "..(activelow and "not " or "").."Gate.getportstate(gate, "..clockport..")~=0 then",
	}
	for i = 1, bits do
		table.insert(tfunc, "		Gate.setportstate(gate, "..(i+bits)..", Gate.getportstate(gate, "..i.."))")
	end
	if kind=="buf" or kind=="ena" then
		table.insert(tfunc, "	else")
		for i = 1, bits do
			table.insert(tfunc, "		Gate.setportstate(gate, "..(i+bits)..", 0)")
		end
	end
	table.insert(tfunc, "	end")
	table.insert(tfunc, "end")
	
	return LogicStrFromTable(tfunc)
end

local function genBus(kind, num, activelow, dir)
	local data = {}
	
	local names = {["dff"] = "DFlipFlop" , ["buf"] = "Buffer", ["ena"] = "Enabler"}
	local uis   = {["dff"] = "D FlipFlop", ["buf"] = "Buffer", ["ena"] = "Enabler"}
	local dirs  = {["h"] = "", ["u"] = "Up", ["d"] = "Down"}
	
	local dbname = "LogicGate_"..names[kind]..(activelow and "Al"          or "")..     num.. "Bit"..dirs[dir].."_Data"
	local uiname = uis  [kind]..(activelow and " Active Low" or "").." "..num.." Bit"..(dirs[dir]~="" and " "..dirs[dir] or "")
	local subcat = "Bus"
	
	GenDataStandard(data, dbname, uiname, subcat)
	GenDataGate(data)
	
	data.logicBrickSize = {num, 1, 1}
	data.orientationFix = 3
	
	data.logicUpdate = logicUpdateGenBus(num, kind, activelow)
	
	data.numLogicPorts = num*2+1
	
	local dirs_in  = {["h"] = 3, ["u"] = 5, ["d"] = 4}
	local dirs_out = {["h"] = 1, ["u"] = 4, ["d"] = 5}
	
	data.ports = {}
	for i = 1, num do
		local pos = {(num-1) - (i-1)*2, 0, 0}
		data.ports[i] = {
			type = 1,
			pos = pos,
			dir = dirs_in[dir],
			name = "In"..(i-1),
			upd = (kind=="ena"),
		}
		data.ports[i+num] = {
			type = 0,
			pos = pos,
			dir = dirs_out[dir],
			name = "Out"..(i-1),
		}
	end
	data.ports[num*2+1] = {
		type = 1,
		pos = {num-1, 0, 0},
		dir = 2,
		name = "Clock",
		upd = true,
	}
	
	return data
end


local bussizes = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,24,32,48,64}
local buskinds = {"dff", "buf", "ena"}
--local busals = {false, true}
local busals = {false}
local busdirs = {"h", "u", "d"}
for kidx, kind in ipairs(buskinds) do
	for aidx, al in ipairs(busals) do
		for idx, bits in ipairs(bussizes) do
			for didx, dir in ipairs(busdirs) do
				OutputBrickData(genBus(kind, bits, al, dir))
			end
		end
	end
end
