
local function logicUpdateGenAdder(bits)
	local addt = {}; local function add(x) table.insert(addt, x) end
	
	add("return function(gate)")
	add("local val = (")
	add("   ( Gate.getportstate(gate, 1) + Gate.getportstate(gate, "..(bits+1)..") + Gate.getportstate(gate, "..(bits*3+1).."))")
	for i = 1, bits-1 do add(" + ((Gate.getportstate(gate, "..(i+1)..") + Gate.getportstate(gate, "..(bits+i+1)..")) * "..math.pow(2, i)..")") end
	add(")")
	
	for i = 1, bits+1 do
		local p = math.pow(2, bits+1-i); local g = i==1 and (bits*3+2) or (bits*3+2-i);
		add("if val >= "..p.." then val = val-"..p.."; Gate.setportstate(gate, "..g..", 1); else Gate.setportstate(gate, "..g..", 0) end")
	end
	add("end")
	
	return LogicStrFromTable(addt)
end

local function genAdder(bits)
	local data = {}
	
	local dbname = "LogicGate_Adder"..bits.. "Bit_Data"
	local uiname = "Adder " ..bits.." Bit"
	local subcat = "Math"
	
	GenDataStandard(data, dbname, uiname, subcat)
	GenDataGate(data)
	
	data.logicBrickSize = {bits*2, 2, 1}
	data.orientationFix = 3
	
	data.logicUpdate = logicUpdateGenAdder(bits, op)
	
	data.numLogicPorts = bits*3 + 2
	
	data.ports = {}
	for i = 1, bits do
		data.ports[i] = { -- Input A
			type = Input,
			pos = {(bits*2-1) - (i-1)*2, -1, 0},
			dir = Toward,
			name = "A"..(i-1),
			upd = true,
		}
		data.ports[i+bits] = { -- Input B
			type = Input,
			pos = {-1 - (i-1)*2, -1, 0},
			dir = Toward,
			name = "B"..(i-1),
			upd = true,
		}
		data.ports[i+bits*2] = { -- Output
			type = Output,
			pos = {(bits*2-1) - (i-1)*2, 1, 0},
			dir = Away,
			name = "O"..(i-1),
		}
	end
	data.ports[bits*3+1] = { -- CIn
		type = Input,
		pos = {bits*2-1, -1, 0},
		dir = Right,
		name = "CIn",
		upd = true,
	}
	data.ports[bits*3+2] = { -- COut
		type = Output,
		pos = {-(bits*2-1), -1, 0},
		dir = Left,
		name = "COut",
		upd = true,
	}
	
	return data
end

local mathbits = {1, 2, 4, 8, 16, 32}
for bidx, bits in ipairs(mathbits) do
	OutputBrickData(genAdder(bits))
	--OutputBrickData(genMath(bits, "Adder"     , "$in1 + $in2"                      , nil                      ))
	--OutputBrickData(genMath(bits, "Subtractor", "$in1 - $in2"                      , nil                      ))
	--OutputBrickData(genMath(bits, "Multiplier", "$in1 * $in2 % "..math.pow(2, bits), "math.floor($in1 * $in2)"))
	--OutputBrickData(genMath(bits, "Divider"   , "math.floor($in1 / $in2)"          , "$in1 % $in2"            ))
end
