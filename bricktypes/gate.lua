
local function logicUpdateGenGate(size, kind)
	
	local fkind = nil
	local inv = false
	if kind=="and" or kind=="or" then
		fkind = kind
	elseif kind=="nand" then
		fkind = "and"
		inv = true
	elseif kind=="nor" then
		fkind = "or"
		inv = true
	end
	
	local tfunc = {"return function(gate)"}
	
	if fkind then -- if and/or/nand/nor
		table.insert(tfunc, "	Gate.setportstate(gate, "..(size+1)..", ("..(inv and "not " or "").."(")
		for i = 1, size-1 do
			table.insert(tfunc, "		(Gate.getportstate(gate, "..i..")~=0) "..fkind)
		end
		table.insert(tfunc, "		(Gate.getportstate(gate, "..size..")~=0)")
		table.insert(tfunc, "	)) and 1 or 0)")
		
	elseif kind=="xor" or kind=="xnor" then
		table.insert(tfunc, "	local v = "..(kind=="xnor" and "1" or "0"))
		for i = 1, size do
			table.insert(tfunc, "	if Gate.getportstate(gate, "..i..")~=0 then v = 1 - v end")
		end
		table.insert(tfunc, "	Gate.setportstate(gate, "..(size+1)..", v)")
	end
	
	table.insert(tfunc, "end")
	
	return LogicStrFromTable(tfunc)
end

local function genGate(kind, size, vert)
	local data = {}
	
	local ui = kind:upper()
	local name = kind:sub(1,1):upper()..kind:sub(2,#kind)
	
	local dbname = "LogicGate_Gate"..name..size..(vert and "Vertical" or "").."_Data"
	local uiname = ""..ui.." "..size.." Bit"..(vert and " Vertical" or "")
	local subcat = "Gates"
	
	GenDataStandard(data, dbname, uiname, subcat)
	GenDataGate(data)
	
	data.logicBrickSize = vert and {1, 1, size} or {size, 1, 1}
	data.orientationFix = 3
	
	data.logicUpdate = logicUpdateGenGate(size, kind)
	
	data.numLogicPorts = size+1
	
	data.ports = {}
	for i = 1, size do
		data.ports[i] = {
			type = 1,
			pos = vert and {0, 0, (size-1)-(i-1)*2} or {(size-1)-(i-1)*2, 0, 0},
			dir = 3,
			name = "In"..(i-1),
			upd = true,
		}
	end
	data.ports[size+1] = {
		type = 0,
		pos = vert and {0, 0, -(size-1)} or {size-1, 0, 0},
		dir = 1,
		name = "Out",
	}
	
	return data
end


local gatesizes = {2,3,4,5,6,7,8}
local gatekinds = {"and", "or", "xor", "nand", "nor", "xnor"}
local gateverts = {false, true}
for vidx, vert in ipairs(gateverts) do
	for szidx, size in ipairs(gatesizes) do
		for kdidx, kind in ipairs(gatekinds) do
			OutputBrickData(genGate(kind, size, vert))
		end
	end
end
