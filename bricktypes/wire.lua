
local function brickSizeToName(size)
	local lw = size[1].."x"..size[2]
	local h = size[3]
	if h==1 then
		return lw.."f"
	elseif h%3==0 then
		if h==3 then return lw
		else         return lw.."x"..(h/3)
		end
	else
		return lw.."x"..h.."f"
	end
end

local function genWire(size, subcatext)
	local data = {}
	
	local szstr = brickSizeToName(size)
	local dbname = "LogicWire_"..szstr.."_Data"
	local uiname = "Wire "..szstr..""
	local subcat = "Wires "..subcatext
	
	GenDataStandard(data, dbname, uiname, subcat)
	GenDataWire(data)
	
	data.logicBrickSize = size
	
	return data
end


local wirelengths = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64}
local wireheights = {2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,56,64,65,66,72,80,96,112,128,144,160,256}
local wireothers  = { {1,2,5}, {64,64,1}, {2,2,1}, {4,4,1} }
for idx, len in ipairs(wirelengths) do
	OutputBrickData(genWire({1,len,1}, "Horizontal"))
end
for idx, hei in ipairs(wireheights) do
	OutputBrickData(genWire({1,1,hei}, "Vertical"))
end
for idx, siz in ipairs(wireothers) do
	OutputBrickData(genWire(siz, "Other"))
end
