
local function logicUpdateGenDemux(size)
	local outsize = math.pow(2, size)
	
	local tfunc = {
		"return function(gate)",
		"	if Gate.getportstate(gate, "..(size+outsize+1)..")~=0 then",
	}
	table.insert(tfunc, "		local idx = "..(size+1).." +")
	for i = 1, size-1 do
		table.insert(tfunc, "			(Gate.getportstate(gate, "..i..") * "..math.pow(2, i-1)..") +")
	end
	table.insert(tfunc, "			(Gate.getportstate(gate, "..size..") * "..math.pow(2, size-1)..")")
	
	table.insert(tfunc, "		Gate.setportstate(gate, idx, 1)")
	table.insert(tfunc, "		if gate.laston~=idx then")
	table.insert(tfunc, "			Gate.setportstate(gate, gate.laston, 0)")
	table.insert(tfunc, "			gate.laston = idx")
	table.insert(tfunc, "		end")
	table.insert(tfunc, "	else")
	table.insert(tfunc, "		Gate.setportstate(gate, gate.laston, 0)")
	table.insert(tfunc, "	end")
	table.insert(tfunc, "end")
	
	return LogicStrFromTable(tfunc)
end

local function logicUpdateGenMux(size)
	local outsize = math.pow(2, size)
	
	local tfunc = {
		"return function(gate)",
		"	if Gate.getportstate(gate, "..(size+outsize+1)..") then",
	}
	table.insert(tfunc, "		local idx = "..(size+1).." +")
	for i = 1, size-1 do
		table.insert(tfunc, "			(Gate.getportstate(gate, "..i..") * "..math.pow(2, i-1)..") +")
	end
	table.insert(tfunc, "			(Gate.getportstate(gate, "..size..") * "..math.pow(2, size-1)..")")
	table.insert(tfunc, "		Gate.setportstate(gate, "..(size+outsize+2)..", Gate.getportstate(gate, idx))")
	table.insert(tfunc, "	else")
	table.insert(tfunc, "		Gate.setportstate(gate, "..(size+outsize+2)..", 0)")
	table.insert(tfunc, "	end")
	table.insert(tfunc, "end")
	
	return LogicStrFromTable(tfunc)
end

local function logicInitGenDemux(size)
	local tfunc = {
		"return function(gate)",
		"	gate.laston = "..(size+1),
		"end"
	}
	return LogicStrFromTable(tfunc)
end

local function genDemux(size, vert, ismux, updateall)
	local data = {}
	
	local muxname = ismux and "Mux" or "Demux"
	local dbname = "LogicGate_"..muxname..size..(vert and "Vertical" or "").."_Data"
	local uiname = muxname.." "..size.." Bit"..(vert and " Vertical" or "")
	local subcat = "Mux"
	
	GenDataStandard(data, dbname, uiname, subcat)
	GenDataGate(data)
	
	local outsize = math.pow(2, size)
	
	data.logicBrickSize = (vert and {1, 1, outsize} or {outsize, 1, 1})
	data.orientationFix = 3
	
	data.logicUpdate = ismux and logicUpdateGenMux(size) or logicUpdateGenDemux(size)
	data.logicInit   = (not ismux) and logicInitGenDemux(size)
	
	data.numLogicPorts = size+outsize+(ismux and 2 or 1)
	
	data.ports = {}
	for i = 1, size do -- selection inputs
		data.ports[i] = {
			type = 1,
			pos = (vert and {0, 0, -((outsize-1)-(i-1)*2)} or {(outsize-1)-(i-1)*2, 0, 0}),
			dir = 3,
			name = "Sel"..(i-1),
			upd = updatealways,
		}
	end
	for i = 1, outsize do -- data outputs or inputs
		data.ports[size+i] = {
			type = ismux and 1 or 0,
			pos = (vert and {0, 0, -((outsize-1)-(i-1)*2)} or {(outsize-1)-(i-1)*2, 0, 0}),
			dir = 1,
			name = (ismux and "In" or "Out")..(i-1),
			upd = updatealways and ismux,
		}
	end
	data.ports[size+outsize+1] = { -- enable input
		type = 1,
		pos = (vert and {0, 0, -(outsize-1)} or {outsize-1, 0, 0}),
		dir = (vert and 5 or 2),
		name = "Enable",
		upd = true,
	}
	if ismux then
		data.ports[size+outsize+2] = { -- data output
			type = 0,
			pos = (vert and {0, 0, outsize-1} or {-(outsize-1), 0, 0}),
			dir = (vert and 4 or 0),
			name = "Out",
		}
	end
	
	return data
end


local updateall = false
local muxsizes = {1, 2, 3, 4, 5, 6}
local muxsizesvert = {7, 8}
for szidx, size in ipairs(muxsizes) do
	OutputBrickData(genDemux(size, false, false, updateall))
	OutputBrickData(genDemux(size, true , false, updateall))
	OutputBrickData(genDemux(size, false, true , updateall))
	OutputBrickData(genDemux(size, true , true , updateall))
end
for szidx, size in ipairs(muxsizesvert) do
	OutputBrickData(genDemux(size, true, false, updateall))
	OutputBrickData(genDemux(size, true, true , updateall))
end
