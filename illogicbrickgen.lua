
local brickDataToBlb = require "brickDataToBlb"
local brickDataToCode = require "brickDataToCode"

--------------------------------------------------------------

Output = 0
Input = 1

Left = 0
Right = 2
Toward = 3
Away = 1

function Log2(n) return math.log(n)/math.log(2) end
function IsPower2(n) return Log2(n)%1==0 end

local function unquote(s)
	return s:match("^\"(.*)\"$")
end

local function expandescape(s)
	s = s:gsub("\\", "\\\\")
	s = s:gsub("\"", "\\\"")
	s = s:gsub("\'", "\\\'")
	return s
end

function LogicStrFromTable(tfunc)
	for i, v in ipairs(tfunc) do
		tfunc[i] = expandescape(v)
	end
	
	return "\n\t\t\""..table.concat(tfunc, " \" @\n\t\t\"").."\"\n\t"
end

function LogicStrFromFile(fn, vars)
	local fi = io.open(fn, "rb")
	assert(fi, "cannot open "..fn)
	local text = fi:read("*a")
	fi:close()
	
	text = text:gsub("$([a-zA-Z0-9_]+)", function(varname)
		assert(vars[varname], "undefined variable "..varname)
		return vars[varname]
	end)
	
	local lines = {}
	for line in text:gmatch("[^\r\n]+") do
		table.insert(lines, line)
	end
	
	return LogicStrFromTable(lines)
end

function CodeStrFromFile(fn, vars)
	local fi = io.open(fn, "rb")
	assert(fi, "cannot open "..fn)
	local text = fi:read("*a")
	fi:close()
	
	text = text:gsub("$([a-zA-Z0-9_]+)", function(varname)
		assert(vars[varname], "undefined variable "..varname)
		return vars[varname]
	end)
	
	return text
end

--------------------------------------------------------------

local enablewrite = false
local function writeToFile(filename, text)
	if enablewrite then
		local outfile = io.open(filename, "wb")
		outfile:write(text)
		outfile:close()
	end
end

local execlines = false
local function outputStart(ew)
	enablewrite = ew
	execlines = {}
end

local function outputStop()
	table.insert(execlines, "")
	local execstr = table.concat(execlines, "\n")
	writeToFile("output/execall.cs", execstr)
end

function OutputBrickData(data)
	print(data.uiName)
	
	local code = brickDataToCode(data)
	writeToFile("output/newcode/"..unquote(data.uiName)..".cs", code)
	
	local blb = brickDataToBlb(data)
	writeToFile("output/newbricks/"..unquote(data.uiName)..".blb", blb)
	
	local execstr = "exec(\"./newcode/"..unquote(data.uiName)..".cs\");"
	table.insert(execlines, execstr)
end

--------------------------------------------------------------

local function firstword(s) return s:match("^[^ ]+") end

function GenDataStandard(data, dbname, uiname, subcat)
	data.dbname = dbname
	
	data.brickFile = "$LuaLogic::Path @ \"bricks/gen/newbricks/"..uiname..".blb\""
	data.iconName = "$LuaLogic::Path @ \"bricks/gen/newicons/"..uiname.."\""
	--data.iconName = "$LuaLogic::Path @ \"icons/"..firstword(uiname).."\""
	
	data.category = "\"Logic Bricks\""
	data.subCategory = "\""..subcat.."\""
	data.uiName = "\""..uiname.."\""
	
	data.isLogic = true
end

function GenDataGate(data)
	data.logicUIName = data.uiName
	data.logicUIDesc = "\"\""
	
	data.hasPrint = 1
	data.printAspectRatio = "\"Logic\""
	
	data.isLogicGate = true
end

function GenDataWire(data)
	data.isLogicWire = true
end

--------------------------------------------------------------

outputStart(true)

--require("bricktypes.diode")

require("bricktypes.wire")
require("bricktypes.gate")
require("bricktypes.bus")
require("bricktypes.demux")
require("bricktypes.rom")
require("bricktypes.math")

outputStop()

print("Done")
