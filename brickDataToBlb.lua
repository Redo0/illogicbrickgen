
local function gen_blb(db)
	if db.isLogicWire then
		local sz = db.logicBrickSize
		return sz[1].." "..sz[2].." "..sz[3].."\nBRICK"
	end
	
	local getportsbydir = function(dir)
		ports = {}
		for i, port in ipairs(db.ports) do
			if port.dir == dir then
				table.insert(ports, port)
			end
		end
		
		return ports
	end
	
	local outfile_name = db.brickFile:match("^.+/(.+)$")
	--if outfile_name == "output.blb" or outfile_name == "input.blb" then
	--	outfile_name = "res_" .. outfile_name
	--end
	
	--local outfile = io.open(outfile_name, "w")
	local outfile = {}
	local outlines = {}
	outfile.writel = function(self, line)
		--self:write(line .. "\n")
		table.insert(outlines, line)
	end
	
	outfile.writeport = function(self, port)
		self:writel("")
		
		local filenames = {"templatebricks/output.blb", "templatebricks/input.blb"}
		local filename = filenames[port.type+1]
		local fileh = io.open(filename, "rb")
		
		local write = false
		local ispos = false
		for line in fileh:lines() do
			if write then
				if string.find(line, "//end") ~= nil then
					break
				elseif string.find(line, "POSITION:") ~= nil then
					ispos = true
				elseif string.find(line, "UV COORDS:") ~= nil then
					ispos = false
				elseif ispos then
					local x, y, z = line:match("(%g+) (%g+) (%g+)")
					line = tonumber(x) + port.pos[1]/2 .. " " .. tonumber(y) + port.pos[2]/2 .. " " .. tonumber(z) + port.pos[3]/2
				end
				
				self:writel(line)
			elseif string.find(line, "//DIR " .. port.dir) ~= nil then
				write = true
			end
		end
		fileh:close()
	end
	
	local x = db.logicBrickSize[1]
	local y = db.logicBrickSize[2]
	local z = db.logicBrickSize[3]
	
	local nquads = getportsbydir(1)
	local equads = getportsbydir(2)
	local squads = getportsbydir(3)
	local wquads = getportsbydir(0)
	local tquads = getportsbydir(4)
	local bquads = getportsbydir(5)
	
	outfile:writel(x .. " " .. y .. " " .. z)
	outfile:writel("SPECIAL\n")
	
	local b = string.rep("b", x)
	local u = string.rep("u", x)
	local X = string.rep("X", x)
	local d = string.rep("d", x)
	
	for i = 0, y-1 do
		if z < 2 then
			outfile:writel(b .. "\n")
		elseif z == 2 then
			outfile:writel(u)
			outfile:writel(d .. "\n")
		else
			outfile:writel(u)
			
			for j = 0, z-3 do
				outfile:writel(X)
			end
			
			outfile:writel(d .. "\n")
		end
	end
	
	--Collision
	outfile:writel("1\n");
	outfile:writel("0 0 0");
	outfile:writel(x .. " " .. y .. " " .. z);
	
	--Coverage
	outfile:writel("COVERAGE:");
	outfile:writel("1 : " .. x * y);
	outfile:writel("1 : " .. x * y);
	outfile:writel("1 : " .. x * z);
	outfile:writel("1 : " .. y * z);
	outfile:writel("1 : " .. x * z);
	outfile:writel("1 : " .. y * z);
	
	--Top quads
	outfile:writel("----------------top quads:");
	outfile:writel(1+(#tquads*5));
	
	outfile:writel("");
	outfile:writel("TEX:PRINT");
	outfile:writel("POSITION:");
	outfile:writel(0.5 * x .. " " .. 0.5 * y .. " " .. z * 0.5);
	outfile:writel(0.5 * x .. " " .. -0.5 * y .. " " .. z * 0.5);
	outfile:writel(-0.5 * x .. " " .. -0.5 * y .. " " .. z * 0.5);
	outfile:writel(-0.5 * x .. " " .. 0.5 * y .. " " .. z * 0.5);
	
	outfile:writel("UV COORDS:");
	--Standard top print texture
	-- outfile:writel("0" .. " " .. y);
	-- outfile:writel("0 0");
	-- outfile:writel(x .. " " .. "0");
	-- outfile:writel(x .. " " .. y);
	outfile:writel("1 0");
	outfile:writel("1 1");
	outfile:writel("0 1");
	outfile:writel("0 0");
	
	outfile:writel("NORMALS:");
	outfile:writel("0 0 1");
	outfile:writel("0 0 1");
	outfile:writel("0 0 1");
	outfile:writel("0 0 1");
	
	for i, port in ipairs(tquads) do
		outfile:writeport(port)
	end
	
	--Bottom quads
	outfile:writel("----------------bottom quads:");
	
	if x > 1 and y > 1 then
		outfile:writel(5+(#bquads*5));
		
		outfile:writel("");
		outfile:writel("TEX:BOTTOMLOOP");
		outfile:writel("POSITION:");
		outfile:writel(x / 2 - 0.5 .. " " .. -0.5 * y + 0.5 .. " " .. z * -0.5);
		outfile:writel(x / 2 - 0.5 .. " " .. y / 2 - 0.5 .. " " .. z * -0.5);
		outfile:writel(-0.5 * x + 0.5 .. " " .. y / 2 - 0.5 .. " " .. z * -0.5);
		outfile:writel(-0.5 * x + 0.5 .. " " .. -0.5 * y + 0.5 .. " " .. z * -0.5);
		outfile:writel("UV COORDS:");
		outfile:writel("0 0");
		outfile:writel("0" .. " " .. y - 1);
		outfile:writel(x - 1 .. " " .. y - 1);
		outfile:writel(x - 1 .. " " .. "0");
		outfile:writel("NORMALS:");
		outfile:writel("0 0 -1");
		outfile:writel("0 0 -1");
		outfile:writel("0 0 -1");
		outfile:writel("0 0 -1");
	else
		outfile:writel(4+(#bquads*5));
	end
	
	outfile:writel("");
	outfile:writel("TEX:BOTTOMEDGE");
	outfile:writel("POSITION:");
	outfile:writel(-0.5 * x .. " " .. -0.5 * y .. " " ..  z * -0.5);
	outfile:writel(0.5 * x .. " " .. -0.5 * y .. " " ..  z * -0.5);
	outfile:writel(0.5 * x - 0.5 .. " " .. -0.5 * y + 0.5 .. " " ..  z * -0.5);
	outfile:writel(-0.5 * x + 0.5 .. " " .. -0.5 * y + 0.5 .. " " ..  z * -0.5);
	outfile:writel("UV COORDS:");
	outfile:writel("-0.5 0");
	outfile:writel(x - 0.5 .. " " .. "0");
	outfile:writel(x - 1 .. " " .. "0.5");
	outfile:writel("0 0.5");
	outfile:writel("NORMALS:");
	outfile:writel("0 0 -1");
	outfile:writel("0 0 -1");
	outfile:writel("0 0 -1");
	outfile:writel("0 0 -1");
	
	outfile:writel("");
	outfile:writel("TEX:BOTTOMEDGE");
	outfile:writel("POSITION:");
	outfile:writel(0.5 * x .. " " .. 0.5 * y .. " " .. z * -0.5);
	outfile:writel(-0.5 * x .. " " .. 0.5 * y .. " " .. z * -0.5);
	outfile:writel(-0.5 * x + 0.5 .. " " .. 0.5 * y - 0.5 .. " " .. z * -0.5);
	outfile:writel(0.5 * x - 0.5 .. " " .. 0.5 * y - 0.5 .. " " .. z * -0.5);
	outfile:writel("UV COORDS:");
	outfile:writel("-0.5 0");
	outfile:writel(x - 0.5 .. " " .. "0");
	outfile:writel(x - 1 .. " " .. "0.5");
	outfile:writel("0 0.5");
	outfile:writel("NORMALS:");
	outfile:writel("0 0 -1");
	outfile:writel("0 0 -1");
	outfile:writel("0 0 -1");
	outfile:writel("0 0 -1");
	
	outfile:writel("");
	outfile:writel("TEX:BOTTOMEDGE");
	outfile:writel("POSITION:");
	outfile:writel(0.5 * x .. " " .. -0.5 * y .. " " .. z * -0.5);
	outfile:writel(0.5 * x .. " " .. 0.5 * y .. " " .. z * -0.5);
	outfile:writel(0.5 * x - 0.5 .. " " .. 0.5 * y - 0.5 .. " " .. z * -0.5);
	outfile:writel(0.5 * x - 0.5 .. " " .. -0.5 * y + 0.5 .. " " .. z * -0.5);
	outfile:writel("UV COORDS:");
	outfile:writel("-0.5 0");
	outfile:writel(y - 0.5 .. " " .. "0");
	outfile:writel(y - 1 .. " " .. "0.5");
	outfile:writel("0 0.5");
	outfile:writel("NORMALS:");
	outfile:writel("0 0 -1");
	outfile:writel("0 0 -1");
	outfile:writel("0 0 -1");
	outfile:writel("0 0 -1");
	
	outfile:writel("");
	outfile:writel("TEX:BOTTOMEDGE");
	outfile:writel("POSITION:");
	outfile:writel(-0.5 * x .. " " .. 0.5 * y .. " " .. z * -0.5);
	outfile:writel(-0.5 * x .. " " .. -0.5 * y .. " " .. z * -0.5);
	outfile:writel(-0.5 * x + 0.5 .. " " .. -0.5 * y + 0.5 .. " " .. z * -0.5);
	outfile:writel(-0.5 * x + 0.5 .. " " .. 0.5 * y - 0.5 .. " " .. z * -0.5);
	outfile:writel("UV COORDS:");
	outfile:writel("-0.5 0");
	outfile:writel(y - 0.5 .. " " .. "0");
	outfile:writel(y - 1 .. " " .. "0.5");
	outfile:writel("0 0.5");
	outfile:writel("NORMALS:");
	outfile:writel("0 0 -1");
	outfile:writel("0 0 -1");
	outfile:writel("0 0 -1");
	outfile:writel("0 0 -1");
	
	for i, port in ipairs(bquads) do
		outfile:writeport(port)
	end
	
	--North quad
	outfile:writel("----------------north quads:");
	outfile:writel(1+(#nquads*5));
	
	outfile:writel("");
	outfile:writel("TEX:SIDE");
	outfile:writel("POSITION:");
	outfile:writel(-0.5 * x .. " " .. y / 2 .. " " .. z * 0.5);  ---0.5 0.5
	outfile:writel(-0.5 * x .. " " .. y / 2 .. " " .. z * -0.5); ---0.5 0.5
	outfile:writel(x / 2 .. " " .. y / 2 .. " " .. z * -0.5);       --0.5 0.5
	outfile:writel(x / 2 .. " " .. y / 2 .. " " .. z * 0.5);        --0.5 0.5
	
	outfile:writel("UV COORDS:");
	outfile:writel((-((11 - ((11 / x) * 2)) / 512) + 1) .. " " .. (11 - ((11 / z) * 5)) / 512);
	outfile:writel((-((11 - ((11 / x) * 2)) / 512) + 1) .. " " .. (-((11 - ((11 / z) * 5)) / 512) + 1));
	outfile:writel((11 - ((11 / x) * 2)) / 512 .. " " .. (-((11 - ((11 / z) * 5)) / 512) + 1));
	outfile:writel((11 - ((11 / x) * 2)) / 512 .. " " .. (11 - ((11 / z) * 5)) / 512);
	
	outfile:writel("NORMALS:");
	outfile:writel("0 1 0");
	outfile:writel("0 1 0");
	outfile:writel("0 1 0");
	outfile:writel("0 1 0");
	
	for i, port in ipairs(nquads) do
		outfile:writeport(port)
	end
	
	--East quad
	outfile:writel("----------------east quads:");
	outfile:writel(1+(#equads*5));
	
	outfile:writel("");
	outfile:writel("TEX:SIDE");
	outfile:writel("POSITION:");
	outfile:writel(x / 2 .. " " .. y / 2 - y .. " " .. z * 0.5);
	outfile:writel(x / 2 .. " " .. y / 2 .. " " .. z * 0.5);
	outfile:writel(x / 2 .. " " .. y / 2 .. " " .. z * -0.5);
	outfile:writel(x / 2 .. " " .. y / 2 - y .. " " .. z * -0.5);
	
	outfile:writel("UV COORDS:");
	outfile:writel((11 - ((11 / y) * 2)) / 512 .. " " .. (11 - ((11 / z) * 5)) / 512);
	outfile:writel((-((11 - ((11 / y) * 2)) / 512) + 1) .. " " .. (11 - ((11 / z) * 5)) / 512);
	outfile:writel((-((11 - ((11 / y) * 2)) / 512) + 1) .. " " .. (-((11 - ((11 / z) * 5)) / 512) + 1));
	outfile:writel((11 - ((11 / y) * 2)) / 512 .. " " .. (-((11 - ((11 / z) * 5)) / 512) + 1));
	
	outfile:writel("NORMALS:");
	outfile:writel("1 0 0");
	outfile:writel("1 0 0");
	outfile:writel("1 0 0");
	outfile:writel("1 0 0");
	
	for i, port in ipairs(equads) do
		outfile:writeport(port)
	end
	
	--South quad
	outfile:writel("----------------south quads:");
	outfile:writel(1+(#squads*5));
	
	outfile:writel("");
	outfile:writel("TEX:SIDE");
	outfile:writel("POSITION:");
	outfile:writel(x / 2 .. " " .. y / 2 - y .. " " .. z * 0.5);
	outfile:writel(x / 2 .. " " .. y / 2 - y .. " " .. z * -0.5);
	outfile:writel(x / 2 - x .. " " .. y / 2 - y .. " " .. z * -0.5);
	outfile:writel(x / 2 - x .. " " .. y / 2 - y .. " " .. z * 0.5);
	
	outfile:writel("UV COORDS:");
	outfile:writel((-((11 - ((11 / x) * 2)) / 512) + 1) .. " " .. (11 - ((11 / z) * 5)) / 512);
	outfile:writel((-((11 - ((11 / x) * 2)) / 512) + 1) .. " " .. (-((11 - ((11 / z) * 5)) / 512) + 1));
	outfile:writel((11 - ((11 / x) * 2)) / 512 .. " " .. (-((11 - ((11 / z) * 5)) / 512) + 1));
	outfile:writel((11 - ((11 / x) * 2)) / 512 .. " " .. (11 - ((11 / z) * 5)) / 512);
	
	outfile:writel("NORMALS:");
	outfile:writel("0 -1 0");
	outfile:writel("0 -1 0");
	outfile:writel("0 -1 0");
	outfile:writel("0 -1 0");
	
	for i, port in ipairs(squads) do
		outfile:writeport(port)
	end
	
	--West quad
	outfile:writel("----------------west quads:");
	outfile:writel(1+(#wquads*5));
	
	outfile:writel("");
	outfile:writel("TEX:SIDE");
	outfile:writel("POSITION:");
	outfile:writel(x / 2 - x .. " " .. y / 2 - y .. " " .. z * -0.5);
	outfile:writel(-0.5 * x .. " " .. y / 2 .. " " .. z * -0.5); ---0.5 0.5
	outfile:writel(-0.5 * x .. " " .. y / 2 .. " " .. z * 0.5); ---0.5 0.5
	outfile:writel(x / 2 - x .. " " .. y / 2 - y .. " " .. z * 0.5);
	
	outfile:writel("UV COORDS:");
	outfile:writel((-((11 - ((11 / y) * 2)) / 512) + 1) .. " " .. (-((11 - ((11 / z) * 5)) / 512) + 1));
	outfile:writel((11 - ((11 / y) * 2)) / 512 .. " " .. (-((11 - ((11 / z) * 5)) / 512) + 1));
	outfile:writel((11 - ((11 / y) * 2)) / 512 .. " " .. (11 - ((11 / z) * 5)) / 512);
	outfile:writel((-((11 - ((11 / y) * 2)) / 512) + 1) .. " " .. (11 - ((11 / z) * 5)) / 512);
	
	outfile:writel("NORMALS:");
	outfile:writel("-1 0 0");
	outfile:writel("-1 0 0");
	outfile:writel("-1 0 0");
	outfile:writel("-1 0 0");
	
	for i, port in ipairs(wquads) do
		outfile:writeport(port)
	end
	
	--Omni quads
	outfile:writel("----------------omni quads:");
	outfile:writel("0");
	
	--outfile:close()
	
	return table.concat(outlines, "\n")
end

-- local test_db = {
-- 	["brickFile"] = "blah/blah/ok/testbrick.blb",
-- 	["logicBrickSize"] = {1, 2, 1},
-- 	["ports"] = {
-- 		{["type"] = 1, ["pos"] = {0,  1, 0}, ["dir"] = 0},
-- 		{["type"] = 1, ["pos"] = {0, -1, 0}, ["dir"] = 0},
-- 		{["type"] = 0, ["pos"] = {0, -1, 0}, ["dir"] = 2},
-- 	}
-- }

-- gen_blb(test_db)

return gen_blb
